﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public GameObject weaponPivot;
    public float moveForce = 30f;
    public float maxSpeed = 5f;
    public float jumpForce = 750f;
    public GunController gun;
    public bool isFacingRight;
    public bool iamEnemy = false;
    public Transform groundCheck;
    [HideInInspector]
    public bool hasTurn = false;
    [HideInInspector]
    public bool attackInCourse = false;

    private bool jump = false;
    private bool grounded;
    private Animator playerAnimator;
    private Rigidbody2D playerRigidBody;


    void Awake ()
    {
        playerRigidBody = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponentInChildren<Animator>();
        isFacingRight = true;
    }
	
    
	void Update ()
    {
        //Detect if the player is on the ground in order to change drag
        grounded = GroundDetection(groundCheck);
        if (grounded)
        {
            playerRigidBody.drag = 5f;
            playerAnimator.SetBool("Jump", false);
        }
        else
        {
            playerRigidBody.drag = 0f;
        }
        if (hasTurn)
        {
            if (!iamEnemy)
            {
                grounded = GroundDetection(groundCheck);
                //Check when the player has to flip
                CheckMousePosition();
                if (Input.GetKeyDown(KeyCode.Space) && grounded)
                {
                    jump = true;
                }
                ShootDetection();
            }
        }
	}

    void FixedUpdate()
    {
        if(hasTurn)
        {
            if (!iamEnemy)
            {
                //Check keyboard input in order to move player
                float h = Input.GetAxis("Horizontal");
                playerAnimator.SetFloat("Speed", Mathf.Abs(h));
                if (Mathf.Abs(playerRigidBody.velocity.x) < maxSpeed)
                {
                    playerRigidBody.AddForce(Vector2.right * h * moveForce);
                }
                if (jump)
                {
                    playerAnimator.SetBool("Jump", true);
                    playerRigidBody.AddForce(new Vector2(0f, jumpForce));
                    jump = false;
                }
            }
        }
        else
        {
            playerAnimator.SetFloat("Speed", Mathf.Abs(0));
        }
    }

    public bool GroundDetection(Transform groundChecker)
    {
        return Physics2D.Linecast(transform.position, groundChecker.position, 1 << LayerMask.NameToLayer("Ground"));
    }

    void ShootDetection()
    {
        if(Input.GetMouseButtonDown(0))
        {
            gun.FireDown();
            attackInCourse = true;
        }
        else if(Input.GetMouseButtonUp(0))
        {
            gun.FireUp();
            attackInCourse = false;
        }
    }

    void CheckMousePosition()
    {
        Vector3 mousePosScreen = Input.mousePosition;
        Vector3 mousePosWorld = Camera.main.ScreenToWorldPoint(mousePosScreen);
        //Get position from mouse relative to player in order to know if its + or -
        Vector2 playerToMouse = new Vector2(mousePosWorld.x - transform.position.x,mousePosWorld.y - transform.position.y);
        //Set as magnitude of one
        playerToMouse.Normalize();
        //Get on degrees the rotation angle value
        float angle = Mathf.Asin(playerToMouse.y) * Mathf.Rad2Deg;
        //Limit the values which ones can take the rotation by the player
        angle = Mathf.Clamp(angle, -45, 45);
        if (playerToMouse.x < 0f && transform.localScale.x > 0f)
        {
            Flip();
        }
        else if (playerToMouse.x > 0f && transform.localScale.x < 0f)
        {
            Flip();
        }
        //Rotate pivot which contains hands and gun from player
        weaponPivot.transform.localEulerAngles = new Vector3(0f, 0f, angle);
    }
    /// <summary>
    /// Change orientation from player
    /// </summary>
    public void Flip()
    {
        isFacingRight = !isFacingRight;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }
}
