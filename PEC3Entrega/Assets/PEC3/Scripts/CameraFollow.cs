﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public float speed = 5f;
    public GameObject globalPosition;
    public float cameraDistance = 30.0f;

    private Transform target;
    private Camera mainCamera;
    private bool targetPlayer;

    void Start()
    {
        targetPlayer = false;
        mainCamera = GetComponent<Camera>();
        mainCamera.orthographicSize = 27f;
    }

    public void SetPlayerToFollow(Transform _target)
    {
        if (_target.gameObject.tag == "Player" || _target.gameObject.tag == "Enemy")
        {
            mainCamera.orthographicSize = 15f;
            targetPlayer = true;
        }
        else
        {
            targetPlayer = false;
        }
        target = _target;
    }

    private void Update()
    {
        FollowTarget();
    }

    /// <summary>
    /// Used at the start of the game in order to show the player the global map
    /// </summary>
    public void ShowAllMap()
    {
        SetPlayerToFollow(globalPosition.transform);
    }

    private void FollowTarget()
    {
        if (target != null)
        {
            if(targetPlayer)
            {
                transform.position = new Vector3(target.position.x, 4.7f, transform.position.z);
            }
            else
            {
                transform.position = new Vector3(target.position.x, target.position.y, transform.position.z);
            }
        }
    }
}
