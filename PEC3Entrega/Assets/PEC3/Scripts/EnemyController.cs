﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : PlayerController {

    private bool onGround;
    private Rigidbody2D enemyRigidBody;
    private Transform player;
    private float shootAngle =20;
    private float tilt;

	void Start () {
        enemyRigidBody = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        tilt = Mathf.Clamp(tilt, -45, 45);
    }
	
	void Update ()
    {
        onGround = GroundDetection(groundCheck);
        if(onGround)
        {
            enemyRigidBody.drag = 5f;
        }
        else
        {
            enemyRigidBody.drag = 0f;
        }
    }
    
    /// <summary>
    /// Calculate velocity in order to reach the objective knowing the target position and the angle
    /// </summary>
    /// <param name="target"></param>
    /// <param name="angle"></param>
    /// <returns></returns>
    float CalculateVelocity(Transform target, float angle)
    {
        Vector3 dir = target.position - transform.position;
        float h = dir.y;
        dir.y = 0;
        float dist = dir.magnitude;
        float a = angle * Mathf.Deg2Rad;

        dist += h / Mathf.Tan(a);
        return Mathf.Sqrt(dist * Physics.gravity.magnitude / Mathf.Sin(2 * a)) * Random.Range(1.2f, 1.3f);
    }

    void LateUpdate()
    {
        if(hasTurn)
        {
            if (isFacingRight)
            {
                Flip();
            }
            if (tilt != shootAngle)
            {
                if (tilt > shootAngle)
                {
                    RotateDown();
                }
                else
                {
                    RotateUp();
                }
            }
            else
            {
                gun.Shoot(CalculateVelocity(player, shootAngle));
            }
        }
        else
        {
            //Change randomly the angle in order to make more "live" the enemy
            shootAngle = Random.Range(20, 45);
        }
    }

    void RotateUp()
    {
        tilt += 1.0f;
        weaponPivot.transform.localEulerAngles = new Vector3(0f, 0f, tilt);
    }

    void RotateDown()
    {
        tilt -= 1.0f;
        weaponPivot.transform.localEulerAngles = new Vector3(0f, 0f, tilt);
    }
}
