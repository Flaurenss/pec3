﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour {

    public GameObject powerShootIndicator;
    public Sprite attackBarSprite;
    public GameObject bulletPrefab;
    Transform attackBar;
    public CameraFollow cameraObject;
    public delegate void GunFired();
    public event GunFired gunFired;

    private float bulletSpeed = 0f;
    private enum States { Down, Fire, Up }
    private States state = States.Up;
    private float targetSpeed;

    void Awake () {
        //Set the power bar 
        GameObject attackbarObject = new GameObject("Power");
        attackBar = attackbarObject.transform;
        attackBar.SetParent(powerShootIndicator.transform);
        attackBar.localPosition = Vector3.zero;
        attackBar.localRotation = Quaternion.identity;
        attackBar.localScale = Vector3.up + Vector3.forward;

        SpriteRenderer rend = attackbarObject.AddComponent<SpriteRenderer>();
        rend.sprite = attackBarSprite;
        rend.sortingLayerID = transform.root.GetComponentInChildren<SpriteRenderer>().sortingLayerID;
        rend.sortingOrder = 1;
    }
	
	void Update () {
		//Auto fire the gun if the speed is reached
        if(targetSpeed > 0)
        {
            state = States.Down;
            if(bulletSpeed >= targetSpeed)
            {
                state = States.Fire;
            }
        }

        switch(state)
        {
            case States.Down:
                if (attackBar.localScale.x < 1.2f)
                {
                    bulletSpeed += Time.deltaTime * 30;
                    //Increment the power bar changing its color
                    attackBar.localScale += Vector3.right * 0.01f;
                    attackBar.GetComponent<SpriteRenderer>().color = Color.Lerp(Color.yellow, Color.red, attackBar.localScale.x);
                }
                else
                {
                    state = States.Fire;
                }
                break;
            case States.Fire:
                Shoot();
                state = States.Up;
                break;
            default:
                break;
        }
	}

    public void FireDown()
    {
        state = States.Down;
    }

    public void FireUp()
    {
        if (state == States.Down)
            state = States.Fire;
    }

    public void Shoot()
    {
        GetComponent<AudioSource>().Play();
        Rigidbody2D bulletInstance = Instantiate(bulletPrefab.GetComponent<Rigidbody2D>(), transform.position, transform.rotation) as Rigidbody2D;
        if (transform.root.GetComponent<PlayerController>().isFacingRight)
        {
            bulletInstance.velocity = transform.right.normalized * bulletSpeed;
        }
        else
        {
            bulletInstance.velocity = -transform.right.normalized * bulletSpeed;
        }
        attackBar.localScale = Vector3.up + Vector3.forward;
        //Set ignore tag to shooter in order to no explode the bullet whith its owner
        bulletInstance.GetComponentInChildren<BulletController>().ignoreTag = transform.root.tag;
        targetSpeed = bulletSpeed = 0;
        cameraObject.SetPlayerToFollow(bulletInstance.transform);
        if(gunFired != null)
        {
            gunFired();
        }
    }
    /// <summary>
    /// Used by the IA in order to set the speed that will have the bullet
    /// </summary>
    /// <param name="_targetSpeed"></param>
    public void Shoot(float _targetSpeed)
    {
        targetSpeed = _targetSpeed;
    }
}
