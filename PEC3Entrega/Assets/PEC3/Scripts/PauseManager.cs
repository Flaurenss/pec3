﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour {

    public GameObject pauseMenu;
    public GameObject player;

    // Controls when the user enters on the pause menu
    void Update ()
    {
		if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(!pauseMenu.activeSelf)
            {
                
                Time.timeScale = 0;
                player.GetComponent<PlayerController>().enabled = false;
                pauseMenu.SetActive(true);
            }
            else
            {
                Time.timeScale = 1;
                player.GetComponent<PlayerController>().enabled = true;
                pauseMenu.SetActive(false);
            }
        }
	}

    public void Exit()
    {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
        SceneManager.LoadScene("MainMenuScene");
    }
}
