﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthController : MonoBehaviour
{

    public float health = 100f;
    public SpriteRenderer healthBar;
    public float damageOnHit = 20f;
    public float repeatDamagePeriod = 2f;
    public delegate void PlayerDeath();
    public event PlayerDeath playerDeath;
    public GameObject healthBarObject;

    private Vector3 healthScale;
    private PlayerController playerController;
    private Animator playerAnimator;
    private float lastTimeHit;

    void Awake()
    {
        // Setting up references.
        playerController = GetComponent<PlayerController>();
        playerAnimator = GetComponentInChildren<Animator>();

        // Getting the intial scale of the healthbar
        healthScale = healthBar.transform.localScale;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Explosion")
        {
            if (Time.time > lastTimeHit + repeatDamagePeriod)
            {
                if (health > 0f)
                {
                    TakeDamage();
                    lastTimeHit = Time.time;
                }
            }
        }
        else if(other.gameObject.tag == "MapLimit")
        {
            health = 0f;
            UpdateHealthBar();
        }
    }

    private void Update()
    {
        //Check on update if the player dies in order to be more accurate with the animation prompt
        if(health <= 0)
        {
            playerAnimator.SetTrigger("Dead");
            Collider2D[] cols = GetComponents<Collider2D>();
            foreach (Collider2D c in cols)
            {
                c.isTrigger = true;
            }
            // Move all sprite parts of the player to the back in order to hide it on the sea
            SpriteRenderer[] spr = GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer s in spr)
            {
                s.sortingLayerName = "Default";
                s.sortingOrder = -3;
            }
            var rb = GetComponent<Rigidbody2D>();
            rb.bodyType = RigidbodyType2D.Static;
            healthBarObject.SetActive(false);
            if (playerDeath != null)
            {
                playerDeath();
            }

        }
    }

    void TakeDamage()
    {
        // Reduce the player's health by 10.
        health -= damageOnHit;

        // Update what the health bar looks like.
        UpdateHealthBar();
    }

    void UpdateHealthBar()
    {
        // Set the health bar's colour to proportion of the way between green and red based on the player's health.
        healthBar.material.color = Color.Lerp(Color.green, Color.red, 1 - health * 0.01f);

        // Set the scale of the health bar to be proportional to the player's health.
        healthBar.transform.localScale = new Vector3(healthScale.x * health * 0.01f, 1, 1);
    }
}
