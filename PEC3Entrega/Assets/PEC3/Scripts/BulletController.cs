﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    public GameObject effect;
    public string ignoreTag;

    private Rigidbody2D rb;
    private Transform bulletSpriteTransform;

	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        bulletSpriteTransform = GetComponent<Transform>();
	}
	
	void Update ()
    {
        //Update rotation of the bullet
        Vector2 direction = new Vector2(rb.velocity.x, rb.velocity.y);
        //Get on degrees the rotation angle value
        direction.Normalize();
        float angle = Mathf.Asin(direction.y) * Mathf.Rad2Deg;
        //when facing left and shooting
        if (direction.x < 0f)
        {
            angle = 180 - angle;
        }
        bulletSpriteTransform.localEulerAngles = new Vector3(0f, 0f, angle - 90);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag != ignoreTag)
        {
            Explode(other);
        }
    }

    private void Explode(Collider2D other)
    {
        Destroy(gameObject);
        var cloneExplosion = Instantiate(effect, transform.position, Quaternion.identity);
        Destroy(cloneExplosion, 3f);
        //Add collider in order to simulate explosion force, must be imporved
        GameObject explosionCircle = new GameObject("ExplosionCircle");
        //Same position as explosion fx
        explosionCircle.transform.position = cloneExplosion.transform.position;
        explosionCircle.tag = "Explosion";
        Destroy(explosionCircle, 0.3f);
        CircleCollider2D explosionRadius = explosionCircle.AddComponent<CircleCollider2D>();
        explosionRadius.radius = 2.5f;
        GameplayManager.bulletExplosion = true;
        
    }
}
