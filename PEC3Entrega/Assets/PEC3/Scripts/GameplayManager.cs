﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{
    public EnemyController enemy;
    public PlayerController player;
    public static bool bulletExplosion;
    public Text timeText;
    public Image clockImage;
    public CameraFollow cameraObject;
    public GameObject endGameMenu;

    private enum LastShooter { Player, Enemy };
    private LastShooter lastShooter;
    private int remainingTime = 10;
    private bool endGame = false;

    void Start()
    {
        enemy.GetComponentInChildren<GunController>().gunFired += DisableEnemy;
        player.GetComponentInChildren<GunController>().gunFired += DisablePlayer;
        player.GetComponent<HealthController>().playerDeath += GameOver;
        enemy.GetComponent<HealthController>().playerDeath += WinGame;
        EnterGame();
    }

    void Update()
    {
        if (bulletExplosion)
        {
            SwapTurn();
            bulletExplosion = false;
        }
    }

    public void EnterGame()
    {
        StartCoroutine(StartGame());
    }

    /// <summary>
    /// Shows the map at the start of the game from outside in order to help the player to get oriented
    /// </summary>
    /// <returns></returns>
    IEnumerator StartGame()
    {
        cameraObject.ShowAllMap();
        yield return new WaitForSeconds(2f);
        enemy.hasTurn = false;
        player.hasTurn = true;
        timeText.text = remainingTime.ToString();
        cameraObject.SetPlayerToFollow(player.transform);
        InvokeRepeating("DecreaseTime", 0, 1);
    }

    IEnumerator SwapTurnCoroutine()
    {
        remainingTime = 10;
        if (!enemy.hasTurn)
        {
            yield
            return new WaitForSeconds(1.0f);
        }
        if (lastShooter == LastShooter.Enemy)
        {
            clockImage.enabled = true;
            timeText.enabled = true;
            player.hasTurn = true;
        }
        cameraObject.SetPlayerToFollow(player.hasTurn ? player.transform : enemy.transform);
        if (lastShooter == LastShooter.Player)
        {
            enemy.hasTurn = true;
        }
    }

    public void SwapTurn()
    {
        StartCoroutine(SwapTurnCoroutine());
    }

    void DecreaseTime()
    {
        if (player.hasTurn && !endGame)
        {
            clockImage.enabled = true;
            timeText.enabled = true;
            if(!player.attackInCourse)
            {
                remainingTime--;
            } 
        }
        timeText.text = remainingTime.ToString();
        if (remainingTime <= 0)
        {
            if (player.hasTurn)
            {
                DisablePlayer();
            }
            else
            {
                DisableEnemy();
            }
            SwapTurn();
        }
    }

    public void DisableEnemy()
    {
        enemy.hasTurn = false;
        lastShooter = LastShooter.Enemy;
    }

    public void DisablePlayer()
    {
        clockImage.enabled = false;
        timeText.enabled = false;
        player.hasTurn = false;
        lastShooter = LastShooter.Player;
    }

    public void GameOver()
    {
        endGame = true;
        DisablePlayers();
        endGameMenu.SetActive(true);
        endGameMenu.GetComponentInChildren<Text>().color = Color.red;
        endGameMenu.GetComponentInChildren<Text>().text = "DEFEATED";
    }

    public void WinGame()
    {
        endGame = true;
        DisablePlayers();
        endGameMenu.SetActive(true);
        endGameMenu.GetComponentInChildren<Text>().color = Color.green;
        endGameMenu.GetComponentInChildren<Text>().text = "VICTORY";
    }

    public void DisablePlayers()
    {
        clockImage.enabled = false;
        timeText.enabled = false;
        player.GetComponent<PlayerController>().enabled = false;
        enemy.GetComponent<EnemyController>().enabled = false;
    }
}

