# Introduction

Welcome to "Island War", an artillery game that contains some references of classics such as "Worms" or "Scorched Earth."
The objective of the game is to reduce the life of the enemy to 0 before he does it from turn to turn and also try not to fall into the sea, since it would mean instant death.

## Game instructions

Each game initially shows the general screen to give some perspective to the player and that this is oriented, after a few seconds the camera will be centered on the player and will have 10 seconds to make his attack, otherwise the turn will become of the enemy
The player must use the mouse to orient the player and the movement keys (A, D or arrows) to move the character and thus avoid falling into the ocean through explosions caused by the enemy. You can also make a slight jump.
Once the player or the enemy is reduced to 0 a screen will appear showing the outcome of the game.

The game can be paused at any time, allowing the player to rest or return to the main menu.

## Dev information

The art of the game has been developed by myself (except the waves that are used from the template project).
The main idea of this project was to implement a destructible terrain as done in the game "Worms", but since I finally did not finish having a result of my liking (the colliders update after the destruction of the pixels was too slow ) and in order to offer a gaming experience with the least possible failures, I decided to change the focus of the project.

The core of the game lies in the launch of the missiles and the movement of the player on the ground.
In the GunController class it is checked at all times when any of the players (either AI or player) is in the process of firing; This is shown visually with a sprite that if it increases while varying color in order to show the user the power that entails (calculating the power according to the time it takes to shoot).

Another important class is BulletController, since it is responsible for detonating the bullet and showing an explosion made with the particle system, such as rotating the bullet to give the sensation of parabolic shooting. The latter is carried out in the Update method of that class, while checking whether the bullet is going in the positive or negative direction on the x-axis.

Finally we have the movement of the player, in the PlayerController class we find the CheckMousePosition method which is responsible for starting from the position of the mouse with respect to the player's character, allowing the weapon to rotate a number of degrees, as well as turning said player. In this way we have tried to add more dynamism to each game, making the player's throws much faster to execute thanks to this way.

This is just a sample prototype. 